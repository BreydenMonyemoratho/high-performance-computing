#include <stdio.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string.h> 
#include <cstdlib>
#include "dcdplugin.c"
#include <cmath>
#include <time.h>
#include <bits/stdc++.h>
#include <omp.h>
#include <sstream> 
#include <set> 
#include <iterator> 

#include <time.h>


using namespace std;
struct Interval 
{ 
    int start, end; 
	float distance;
};
void  split(const std::string& s, char delimiter,vector<int> &vect);
std::vector<int> splitRange(const std::string& s, char delimiter);
float distance(float x1, float y1,float z1, float x2, float y2, float z2);
bool compareInterval(Interval i1, Interval i2) ;
// class distanceObj
// { 
//     // Access specifier 
//     public: 
  
//     // Data Members 
//     int i;
// 	int j;
// 	float distance; 
  
//     // Member Functions() 
//     // void printname() 
//     // { 
//     //    cout << "Geekname is: " << geekname; 
//     // } 
// }; 
// An interval has start time and end time  and distance

int main(int argc, char *argv[]) {
	const auto arg = std::string{ "-i" };
	std::string filename = "test"; // you should look for the filename from argv

	std::ifstream inputfile;
	inputfile.open(filename);

	if(inputfile.is_open()){
		string dcdFilename, kValue;

		// ideally you should be getting the next 4 parameters from the text file that the user inputs as CL args
		dcdFilename = "./The_simulation.dcd";
		 kValue = 3;
		 float k=2.00;
		std::string	subsetA = "1,25-28,67,101";
		std::string	subsetB = "101-605";
		//subsetB = "168043-168052";

		//std::vector<std::string> s=
		//split(subsetA,',' );

		//cout << dcdFilename << endl;
		//cout << kValue << endl;
		//cout << subsetA << endl;
		//cout << subsetB << endl;
		inputfile.close();
		
		vector<int> setA, setB;
		//setA = unroll(subsetA); // unroll should convert 1-403 to 1, 2, 3, 4, 5, 6, ...., 403
		//setB = unroll(subsetB);
	    split(subsetA ,',',setA);
		split(subsetB ,',',setB);
		int natoms;
		dcdhandle *dcd; molfile_timestep_t timestep;
		void *v = open_dcd_read(dcdFilename.c_str(), "dcd", &natoms);
		dcd = (dcdhandle *)v;
		cout << "natoms: " << natoms << endl;

		timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
        vector<Interval> toSortArray; 
        float distance_between;
        float *currentA ; 
        float *currentB ;
         Interval pair;
		double start_time = omp_get_wtime();
       #pragma omp parallel for ordered schedule(static)  private(distance_between,toSortArray, pair)  shared(setA,setB) 
		for (int i=0; i<dcd->nsets; i++) { // loog to iterate through the timesteps
			#pragma omp ordered
				{
					int rc = read_next_timestep(v, natoms, &timestep);
				}
				

    

		
		for (int j=0;j<setA.size();j++)
		{	currentA = timestep.coords + 3 * setA[j];
			 for (int h=0;h<setA.size();h++)
			 {	 currentB = timestep.coords + 3 * setB[h];
			 	distance_between=distance(currentA[0], currentA[1],currentA[2], currentB[0],currentB[1], currentB[2]); 
				 // check if its less than k
				 if (distance_between<k)
				 {	// 
					 
				//   distanceObj pair;
				//  pair.i=i;
				//   pair.j=j;
				//   pair.distance=distance_between;
				 
				  pair.start=j;
				  pair.end=h;
				  pair.distance=distance_between;
				  toSortArray.push_back(pair);
				  //cout<< pair.start<<" "<<pair.end<<" "<< pair.distance<<endl;
				
			// safeFloatToInt(a)
			//cout<< j<<" "<<h<<" "<<distance_between<<endl;

				 }

			 }

		}
		// sort the array and print appropriately
		 sort(toSortArray.begin(), toSortArray.end(),compareInterval);
		 // 101,25,3,4.567 format for printing the results 
         #pragma  omp ordered
		 for (auto x : toSortArray) 
        	cout << i<< " " << x.start << " " << x.end <<" "<<x.distance<< endl;

		// //  if(i==10)
        // //     break;  
		}
		 double end_time=omp_get_wtime();
		printf("Time: \t %f \n", end_time-start_time);
	} else {
		std::cout << "Error: File cannot be opened!";
	}

	return 1;
}


void split(const std::string& s, char delimiter,vector<int> &vect)
{	 
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
	  
	   string s2 = "-";
	
	   	// check if it contains - and add the range
     if (strstr(token.c_str(),s2.c_str()))
	 {
		 // adding the range
		vector<int> a=splitRange(token,'-' );
		int i=a[0];
		int j=a[1];
	//	cout<< i<<endl;
		//cout<<j<< endl;

		for ( ; i<=j; i++ ) {
  			 vect.push_back(i);
			//cout<< i<<endl;
		}
		// iterate 
	// 	int end_=a.back();
	//    int begin_=a.front();
	//    cout<<end_<endl;
	//    cout<<begin_<endl;

	 }
	 else
	 {	
		int x = 0;
   	    stringstream geek(token);	
		geek >> x;
		vect.push_back(x);
	    //cout<< x<<endl;
	 }
      
	 
   }
  
}
std::vector<int> splitRange(const std::string& s, char delimiter)
{	 
   std::vector<int> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {	// object from the class stringstream 
	  	int x = 0;
   	    stringstream geek(token);	
		  geek >> x;
	    tokens.push_back(x);
		//cout<<x;
	  
	  
      
	 
   }
   return tokens;
}

float distance(float x1, float y1,float z1, float x2, float y2, float z2)
 {  
     // printf("%.6f", x1);

   float square_difference_x = (x2 - x1) * (x2 - x1);
   float square_difference_y = (y2 - y1) * (y2 - y1);
  float square_difference_z = (z2 - z1) * (z2 - z1);
   float sum = square_difference_x + square_difference_y+square_difference_z;
    
	float value = sqrt(sum);
	return value;
}

// Compares two intervals according to staring times. 
bool compareInterval(Interval i1, Interval i2) 
{ 
    return (i1.distance< i2.distance); 
}

  