#include <stdio.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string.h>
#include <cstdlib>
#include "dcdplugin.c"
#include <cmath>
#include <time.h>
#include <omp.h>
#include <bits/stdc++.h>

#include <sstream>
#include <set>
#include <iterator>


using namespace std;
 /*  elements added to the priority queue
   start= index in setA
   end=index in setB

 */
struct Interval
{
    int start, end;
	float distance;
  // this will used to initialize the variables
 // of the structure
 Interval(int start,int end, float distance)
     : start(start),end(end),distance(distance)
 {
 }
};


void  split(const std::string& s, char delimiter,vector<int> &vect);
std::vector<int> splitRange(const std::string& s, char delimiter);
float distance(float x1, float y1,float z1, float x2, float y2, float z2);
bool compareInterval(Interval i1, Interval i2) ;
void  kNN(molfile_timestep_t timestep, int timestep_index);

/*  comparator for comparing elements of the priority queue by distance*/
struct CompareDistance {
    bool operator()(Interval const& i1, Interval const& i2)
    {
        // return "true" if "p1" is ordered
        // before "p2", for example:
        return i1.distance > i2.distance;
    }
};

		vector<int> setA, setB; // setA indices and setB indices
    float distance_between=0;
    int    KValue = 0;// k value read from the input file
    int number_of_timesteps; // dcd->nsets 
     std::string outputfile; // specified output file
     ofstream outfile; 



int main(int argc, char *argv[]) {
    
    std::string filename = argv[2]; // read the input file from the terminal
    outputfile= argv[4];  // read the output file from the terminal
	const auto arg = std::string{ "-i" };


	std::ifstream inputfile;
	inputfile.open(filename); //opening the file


	if(inputfile.is_open()){
    string dcdFilename, kValueStr;

    // ideally you should be getting the next 4 parameters from the text file that the user inputs as CL args
     inputfile >>	dcdFilename;

    inputfile >> kValueStr;

     stringstream geek(kValueStr); // converts kvalue from string into an int
    geek >> KValue;


    std::string	subsetA ;
     inputfile >>	subsetA;

    std::string	subsetB;
     inputfile >>	subsetB;



		inputfile.close();


		//setA split should convert 1-403 to 1, 2, 3, 4, 5, 6, ...., 403
    	  split(subsetA ,',',setA);
		//setB split should convert 1-403 to 1, 2, 3, 4, 5, 6, ...., 403
    		split(subsetB ,',',setB);


		int natoms;
		dcdhandle *dcd;
		void *v = open_dcd_read(dcdFilename.c_str(), "dcd", &natoms);
		dcd = (dcdhandle *)v;
		cout << "natoms: " << natoms << endl;



      
          int timestep_index=0;

           number_of_timesteps= dcd->nsets;


          double start_time = omp_get_wtime();
         
         outfile.open(outputfile);

                for (int i=0; i<number_of_timesteps; i++) { // loop to iterate through the timesteps // dcd->nsets
                  molfile_timestep_t timestep;
                  timestep.coords = (float *)malloc(3*sizeof(float)*natoms);



                 read_next_timestep(v, natoms, &timestep); // reading from the dcd file
                 timestep_index=i;  // assign the time step to a thread

                  kNN(timestep, timestep_index);

                  free(timestep.coords);
      }
      outfile.close();
      double end_time=omp_get_wtime();
      printf("Time: \t %f \n", end_time-start_time);
}
else{
    cout<<"failed to open the file";
}
return 0;
}
/*computes the nearest neighbour*/
void  kNN(molfile_timestep_t timestep, int timestep_index)
{
        priority_queue<Interval, std::vector<Interval>, CompareDistance>  pq; // priority queue for a specific timestep
        float *currentA, *currentB;
         // start doing the computation
         for (int j=0;j<setA.size();j++)
         {  currentA= timestep.coords + 3 * setA[j];
           for (int k=0;k<setB.size();k++)
            {  currentB= timestep.coords + 3 * setB[k];
               distance_between=distance(currentA[0],currentA[1],currentA[2],currentB[0], currentB[1], currentB[2]);
               pq.push(Interval(setA[j],setB[k] ,distance_between));




            }
         }
         int count=0;
         while (!pq.empty() && count<KValue) {
             Interval p = pq.top();
            pq.pop();
               outfile <<  timestep_index<<" "<< p.start << " "<< p.end<<" " << p.distance << "\n";
              count++;
          }
       

}

void split(const std::string& s, char delimiter,vector<int> &vect)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {

	   string s2 = "-";

	   	// check if it contains - and add the range
     if (strstr(token.c_str(),s2.c_str()))
	 {
		 // adding the range
		vector<int> a=splitRange(token,'-' );
		int i=a[0];
		int j=a[1];
	//	cout<< i<<endl;
		//cout<<j<< endl;

		for ( ; i<=j; i++ ) {
  			 vect.push_back(i);
			//cout<< i<<endl;
		}
		// iterate
	// 	int end_=a.back();
	//    int begin_=a.front();
	//    cout<<end_<endl;
	//    cout<<begin_<endl;

	 }
	 else
	 {
		int x = 0;
   	    stringstream geek(token);
		geek >> x;
		vect.push_back(x);
	    //cout<< x<<endl;
	 }


   }

}
std::vector<int> splitRange(const std::string& s, char delimiter)
{
   std::vector<int> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {	// object from the class stringstream
	  	int x = 0;
   	    stringstream geek(token);
		  geek >> x;
	    tokens.push_back(x);
		//cout<<x;




   }
   return tokens;
}

float distance(float x1, float y1,float z1, float x2, float y2, float z2)
 {
     // printf("%.6f", x1);

   float square_difference_x = (x2 - x1) * (x2 - x1);
   float square_difference_y = (y2 - y1) * (y2 - y1);
  float square_difference_z = (z2 - z1) * (z2 - z1);
   float sum = square_difference_x + square_difference_y+square_difference_z;

	float value = sqrt(sum);
	return value;
}

// Compares two intervals according to staring times.
bool compareInterval(Interval i1, Interval i2)
{
    return (i1.distance< i2.distance);
}
