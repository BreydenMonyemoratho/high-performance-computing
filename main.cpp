#include <stdio.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string.h> 
#include <cstdlib>
#include "dcdplugin.c"
#include <cmath>
#include <time.h>
#include <bits/stdc++.h>
#include <omp.h>
#include <sstream> 
#include <set> 
#include <iterator> 

#include <time.h>


using namespace std;
struct Interval 
{ 
    int start, end; 
	float distance;
};
void  split(const std::string& s, char delimiter,vector<int> &vect);
std::vector<int> splitRange(const std::string& s, char delimiter);
float distance(float x1, float y1,float z1, float x2, float y2, float z2);
bool compareInterval(Interval i1, Interval i2) ;
// class distanceObj
// { 
//     // Access specifier 
//     public: 
  
//     // Data Members 
//     int i;
// 	int j;
// 	float distance; 
  
//     // Member Functions() 
//     // void printname() 
//     // { 
//     //    cout << "Geekname is: " << geekname; 
//     // } 
// }; 
// An interval has start time and end time  and distance

int main(int argc, char *argv[]) {
	const auto arg = std::string{ "-i" };
	std::string filename = "test"; // you should look for the filename from argv

	std::ifstream inputfile;
	inputfile.open(filename);

	if(inputfile.is_open()){
		string dcdFilename, kValue;

		// ideally you should be getting the next 4 parameters from the text file that the user inputs as CL args
		dcdFilename = "./The_simulation.dcd";
		 kValue = 3;
		 float k=2.00;
		std::string	subsetA = "1,25-28,67,101";
		std::string	subsetB = "101-605";
		//subsetB = "168043-168052";

		//std::vector<std::string> s=
		//split(subsetA,',' );

		//cout << dcdFilename << endl;
		//cout << kValue << endl;
		//cout << subsetA << endl;
		//cout << subsetB << endl;
		inputfile.close();
		
		vector<int> setA, setB;
		//setA = unroll(subsetA); // unroll should convert 1-403 to 1, 2, 3, 4, 5, 6, ...., 403
		//setB = unroll(subsetB);
	    split(subsetA ,',',setA);
		split(subsetB ,',',setB);
		int natoms;
		dcdhandle *dcd; molfile_timestep_t timestep;
		void *v = open_dcd_read(dcdFilename.c_str(), "dcd", &natoms);
		dcd = (dcdhandle *)v;
		cout << "natoms: " << natoms << endl;

		timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
		double start_time = omp_get_wtime();
		for (int i=0; i<dcd->nsets; i++) { // loog to iterate through the timesteps
            int rc = read_next_timestep(v, natoms, &timestep);

            // timestep.coords return the address for the x-coord of atom 0
            // (timestep.coords + 1) is the address for the y-coor of atom 0
            // ...
            // (timestep.coords + 3) is the addr of the x-coord of atom 1
            // ....
       		//   int rc = read_next_timestep(raw_data, natoms, &timestep);
       //  if (rc) {
//             read_failed = 1;
//             break;
//         }
        /* At this point 
           dcd contains
           dcd->x = Array of X coordinates of all atoms for timestep i
           dcd->y = Array of Y coordinates of all atoms for timestep i
           dcd->z = Array of Z coordinates of all atoms for timestep i
           
           timestep contains
           timestep.coords = Array of packed XYZ coordinates of all atoms for timestep i
                             [X1, Y1, Z1, X2, Y2, Z2, ..., Xn, Yn, Zn] where n = natoms
          
           Both are overwritten next loop 
        */
        // printf("Timestep %d\n", i);
        // printf("i: x    y      z\n");
        // int n = natoms > 10 ? 10 : natoms;
        // for (int j = 0; j < n; ++j) {
        //     float *current = timestep.coords + 3 * j;
        //     printf("%d: %3.2f %3.2f %3.2f\n", j, current[0], current[1], current[2]);
        // }
        // printf("\n");
        // if (i >= 10) break;
		//vector<distanceObj> toSortArray;

		vector<Interval> toSortArray; 
		for (int j=0;j<setA.size();j++)
		{	float *currentA = timestep.coords + 3 * setA[j];
			 for (int h=0;h<setA.size();h++)
			 {	float *currentB = timestep.coords + 3 * setB[h];
			 	float distance_between=distance(currentA[0], currentA[1],currentA[2], currentB[0],currentB[1], currentB[2]); 
				 // check if its less than k
				 if (distance_between<k)
				 {	// 
					 
				//   distanceObj pair;
				//  pair.i=i;
				//   pair.j=j;
				//   pair.distance=distance_between;
				  Interval pair;
				  pair.start=j;
				  pair.end=h;
				  pair.distance=distance_between;
				  toSortArray.push_back(pair);
				  //cout<< pair.start<<" "<<pair.end<<" "<< pair.distance<<endl;
				
			// safeFloatToInt(a)
			//cout<< j<<" "<<h<<" "<<distance_between<<endl;

				 }

			 }

		}
		// sort the array and print appropriately
		 sort(toSortArray.begin(), toSortArray.end(),compareInterval);
		 // 101,25,3,4.567 format for printing the results 
		 for (auto x : toSortArray) 
        	cout << i<< " " << x.start << " " << x.end <<" "<<x.distance<< endl;

		//  if(i==10)
        //     break;  
		}
		 double end_time=omp_get_wtime();
		  printf("Time: \t %f \n", end_time-start_time);
	} else {
		std::cout << "Error: File cannot be opened!";
	}

	return 1;
}


void split(const std::string& s, char delimiter,vector<int> &vect)
{	 
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
	  
	   string s2 = "-";
	
	   	// check if it contains - and add the range
     if (strstr(token.c_str(),s2.c_str()))
	 {
		 // adding the range
		vector<int> a=splitRange(token,'-' );
		int i=a[0];
		int j=a[1];
	//	cout<< i<<endl;
		//cout<<j<< endl;

		for ( ; i<=j; i++ ) {
  			 vect.push_back(i);
			//cout<< i<<endl;
		}
		// iterate 
	// 	int end_=a.back();
	//    int begin_=a.front();
	//    cout<<end_<endl;
	//    cout<<begin_<endl;

	 }
	 else
	 {	
		int x = 0;
   	    stringstream geek(token);	
		geek >> x;
		vect.push_back(x);
	    //cout<< x<<endl;
	 }
      
	 
   }
  
}
std::vector<int> splitRange(const std::string& s, char delimiter)
{	 
   std::vector<int> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {	// object from the class stringstream 
	  	int x = 0;
   	    stringstream geek(token);	
		  geek >> x;
	    tokens.push_back(x);
		//cout<<x;
	  
	  
      
	 
   }
   return tokens;
}

float distance(float x1, float y1,float z1, float x2, float y2, float z2)
 {  
     // printf("%.6f", x1);

   float square_difference_x = (x2 - x1) * (x2 - x1);
   float square_difference_y = (y2 - y1) * (y2 - y1);
  float square_difference_z = (z2 - z1) * (z2 - z1);
   float sum = square_difference_x + square_difference_y+square_difference_z;
    
	float value = sqrt(sum);
	return value;
}

// Compares two intervals according to staring times. 
bool compareInterval(Interval i1, Interval i2) 
{ 
    return (i1.distance< i2.distance); 
}

  