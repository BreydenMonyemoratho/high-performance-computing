#include <stdio.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string.h>
#include <cstdlib>
#include "dcdplugin.c"
#include <cmath>
#include <time.h>

#include <bits/stdc++.h>

#include <sstream>
#include <set>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>


using namespace std;
struct Interval
{
    int start, end;
	float distance;
};
struct pointsA
{
   float x, y,z;

};
void  split(const std::string& s, char delimiter,vector<int> &vect);
std::vector<int> splitRange(const std::string& s, char delimiter);
float distance(float x1, float y1,float z1, float x2, float y2, float z2);
bool compareInterval(Interval i1, Interval i2) ;


int main(int argc, char *argv[])
{ int myid;                       //holds process's rank id
  int nodenum;
  double start, finish;
  int numberOfTimesteps;
  int KValue;
  std::string filename = argv[2]; // you should look for the filename from argv
  std::string outputfile= argv[4];

  ofstream outfile;
  outfile.open(outputfile);

  start = MPI_Wtime();
  MPI_Init(&argc, &argv);                 //Start MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);           //rank of the calling process in the group of comm (integer) ,Which process am I?
  MPI_Comm_size(MPI_COMM_WORLD, &nodenum);
  if(myid==0)
  {  //std::string filename = "example_input_file1.txt"; // you should look for the filename from argv

    std::ifstream inputfile;
    inputfile.open(filename);

    if(inputfile.is_open())
    {
      string dcdFilename, kValueStr;

      // ideally you should be getting the next 4 parameters from the text file that the user inputs as CL args
       inputfile >>	dcdFilename;


      inputfile >> kValueStr;
      KValue = 0;
       stringstream geek(kValueStr);
      geek >> KValue;




      std::string	subsetA ;
       inputfile >>	subsetA;

      std::string	subsetB;
       inputfile >>	subsetB;
      //    cout<<subsetB<<endl;
       inputfile.close();

      vector<int> setA, setB;
      split(subsetA ,',',setA);
      split(subsetB ,',',setB);
      int natoms;
  		dcdhandle *dcd; molfile_timestep_t timestep;
  		void *v = open_dcd_read(dcdFilename.c_str(), "dcd", &natoms);
  		dcd = (dcdhandle *)v;
  		cout << "natoms: " << natoms << endl;
      numberOfTimesteps=dcd->nsets;

  		timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
     //<dcd->nsets
      bool stopWorker=false;
      int destination=nodenum-1;
      // for(i=n; i>=1; --i) {
      //     //do something with i
      //   }

         for (int q=(nodenum-1);q>0;--q)
        {        // send the number of timesteps
           MPI_Send(&numberOfTimesteps,
                    1,
                    MPI_INT,
                    q,
                    0,
                   MPI_COMM_WORLD);
                  // cout<<q<<endl;

         }
          for (int i=0; i<numberOfTimesteps; i++)
          {       int rc = read_next_timestep(v, natoms, &timestep);
             vector<pointsA> a;
                 vector<pointsA> b;
                // cout<<setA.size()<<endl;
              for ( int j=0;j<setA.size();j++){
                float *currentA= timestep.coords + 3 * setA[j];
                  pointsA a_;
                  a_.x=currentA[0];
                   a_.y=currentA[1];

                  a_.z=currentA[2];
                  a.push_back(a_);


              }
          //   A.push_back(a);
             for ( int j=0;j<setB.size();j++){
                 float *currentB= timestep.coords + 3 * setB[j];
                  // cout<<setB[j]<<endl;
                  pointsA a_;
                  a_.x=currentB[0];
                   a_.y=currentB[1];

                  a_.z=currentB[2];
                  b.push_back(a_);
                //  cout<< a_.x<<" "<<a_.y<<" "<<a_.z<<endl;

              }
            //  B.push_back(b);





            // send the value of k
            MPI_Send(& KValue ,
                     1,
                     MPI_INT,
                     destination,
                     0,
                    MPI_COMM_WORLD);
            //MPI_Send(address, count, datatype, destination, tag, comm)
            // send A with indices

            int setALength=setA.size();
            MPI_Send(&setALength,
                     1,
                     MPI_INT,
                     destination,
                     0,
                      MPI_COMM_WORLD);
            for (int w=0;w<setA.size();w++)
            {
              MPI_Send(&setA[w],
                       1,
                       MPI_INT,
                       destination,
                       0,
                       MPI_COMM_WORLD);

            }

            //MPI_Send(address, count, datatype, destination, tag, comm)
            // send B with indices
            int setBLength=setB.size();
            MPI_Send(&setBLength,
                     1,
                     MPI_INT,
                     destination,
                     0,
                     MPI_COMM_WORLD);
            for (int r=0;r<setB.size();r++)
            {
              MPI_Send(&setB[r],
                       1,
                       MPI_INT,
                       destination,
                       0,
                       MPI_COMM_WORLD);

            }

            //   //MPI_Send(address, count, datatype, destination, tag, comm)
              // send the timesteps
              MPI_Send(&i,
                       1,
                       MPI_INT,
                       destination,
                       0,
                       MPI_COMM_WORLD);
                   // send the size of A
                         int sizeofA=a.size();

                         MPI_Send(&sizeofA,
                                  1,
                                  MPI_INT,
                                  destination,
                                  0,
                                  MPI_COMM_WORLD);

                       //   // send A (x,y,z)
                         for (auto x : a)
                         {     MPI_Send(&x.x,
                                      1,
                                      MPI_FLOAT,
                                      destination,
                                      0,
                                      MPI_COMM_WORLD);
                               MPI_Send(&x.y,
                                               1,
                                               MPI_FLOAT,
                                               destination,
                                               0,
                                               MPI_COMM_WORLD);
                               MPI_Send(&x.z,
                                          1,
                                          MPI_FLOAT,
                                          destination,
                                          0,
                                          MPI_COMM_WORLD);


                         }

                         // send the size of B
                          int sizeofB=b.size();

                         MPI_Send(&sizeofB,
                                   1,
                                   MPI_INT,
                                   destination,
                                  0,
                                   MPI_COMM_WORLD);
                        // send B (x,y,z)
                          for (auto e : b)
                          {     MPI_Send(&e.x,
                                      1,
                                       MPI_FLOAT,
                                       destination,
                                       0,
                                      MPI_COMM_WORLD);
                               MPI_Send(&e.y,
                                                1,
                                              MPI_FLOAT,
                                               destination,
                                               0,
                                                MPI_COMM_WORLD);
                                MPI_Send(&e.z,
                                           1,
                                           MPI_FLOAT,
                                           destination,
                                           0,
                                           MPI_COMM_WORLD);

                          }
                    //  previousDestination=destination;
                    destination=destination-1;
                    if(destination==0 )
                     {  destination=nodenum-1;

                     }
                    // else if (previousDestination==nodenum)
                    // {
                    //   destination=1;
                    // }


                    //cout<< destination<<endl;
                  //  cout<< nodenum<< endl;
          }

    }

  }
  else if(myid!=0)  // worker threads
  { // bool stopWorker=false;
    int NumberOfTimeSteps;
    MPI_Recv(&NumberOfTimeSteps,
           1,
           MPI_INT,
           0,
           0,
           MPI_COMM_WORLD,
           MPI_STATUS_IGNORE);
           // int myInt = ((3-1)*10)/3;
     int s=((myid-1)*NumberOfTimeSteps)/(nodenum-1);
     int e=( myid*NumberOfTimeSteps)/(nodenum-1);
    int n=e-s;

  //cout<<NumberOfTimeSteps<<endl;
    //cout<<n<<endl;
      for(int b=0;b<n;b++)
          {
      //send the number of timesteps


      //  cout<< NumberOfTimeSteps<< endl;
        // receive the value of k
        int KValueWorker;
        MPI_Recv(&KValueWorker,
               1,
               MPI_INT,
               0,
               0,
               MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
        // receive A with indices
        int setASize;
        MPI_Recv(&setASize,
               1,
               MPI_INT,
               0,
               0,
               MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);

        vector<int> FinalSetA;
        for (int y=0;y<setASize;y++)
        {  int index;
          MPI_Recv(&index,
                 1,
                 MPI_INT,
                 0,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
          FinalSetA.push_back(index);


        }
    //
    //     // receive B with indices
        int setBSize;
        MPI_Recv(&setBSize,
               1,
               MPI_INT,
               0,
               0,
               MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);

        vector<int> FinalSetB;
        for (int a=0;a<setBSize;a++)
        { int index;
          MPI_Recv(&index,
                 1,
                 MPI_INT,
                 0,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
            FinalSetB.push_back(index);

        }
    //
    //
    // MPI_Recv(address, maxcount, datatype, source, tag, comm, status)
    // receive the timesteps
      int iteration;
      MPI_Recv(&iteration,
             1,
             MPI_INT,
             0,
             0,
             MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    //  cout<< iteration<<endl;

                    // receive the size of A
                   int sizeofA;
                   MPI_Recv(&sizeofA,
                          1,
                          MPI_INT,
                          0,
                          0,
                          MPI_COMM_WORLD,
                          MPI_STATUS_IGNORE);


                    vector<pointsA> coordinatesA;

                    // receive A
                   for (int k=0;k<sizeofA;k++)
                   {   pointsA a_;
                     MPI_Recv(&a_.x,
                            1,
                            MPI_FLOAT,
                            0,
                            0,
                            MPI_COMM_WORLD,
                            MPI_STATUS_IGNORE);
                      MPI_Recv(&a_.y,
                                   1,
                                   MPI_FLOAT,
                                   0,
                                   0,
                                   MPI_COMM_WORLD,
                                   MPI_STATUS_IGNORE);
                       MPI_Recv(&a_.z,
                                 1,
                                  MPI_FLOAT,
                                 0,
                                   0,
                               MPI_COMM_WORLD,
                                 MPI_STATUS_IGNORE);
                     coordinatesA.push_back(a_);


                   }

                   // receive the size of B
                   int sizeofB;
                   MPI_Recv(&sizeofB,
                          1,
                          MPI_INT,
                          0,
                          0,
                          MPI_COMM_WORLD,
                          MPI_STATUS_IGNORE);

                   vector<pointsA> coordinatesB;
                   // receive  B
                   for (int m=0;m<sizeofB;m++)
                   {   pointsA a_;
                     MPI_Recv(&a_.x,
                            1,
                            MPI_FLOAT,
                            0,
                            0,
                            MPI_COMM_WORLD,
                            MPI_STATUS_IGNORE);
                      MPI_Recv(&a_.y,
                                   1,
                                   MPI_FLOAT,
                                   0,
                                   0,
                                   MPI_COMM_WORLD,
                                   MPI_STATUS_IGNORE);
                       MPI_Recv(&a_.z,
                                 1,
                                  MPI_FLOAT,
                                 0,
                                   0,
                               MPI_COMM_WORLD,
                                 MPI_STATUS_IGNORE);
                     coordinatesB.push_back(a_);
                      //  cout<< a_.x<<" "<<a_.y<<" "<<a_.z<<endl;

                   }
                   //computes the distances.
                  vector<Interval> toSortArray;
                  //  cout<<"worker thread starts here"<<endl;
                  for (int g=0;g<coordinatesA.size();g++)
                  {
                      for (int h=0;h<coordinatesB.size();h++)
                      {
                         float distance_between=distance(coordinatesA[g].x, coordinatesA[g].y,coordinatesA[g].z,coordinatesB[h].x, coordinatesB[h].y,coordinatesB[h].z);

                        //   cout<<coordinatesA[g].x<<" "<<coordinatesA[g].y<<" "<<coordinatesA[g].z<<endl;
                             Interval pair;
                             pair.start=FinalSetA[g];
                             pair.end=FinalSetB[h];
                             pair.distance=distance_between;
                            //
                             toSortArray.push_back(pair);



                      }
                  }
                  // send the number of NumberOfTimeSteps

                  MPI_Send(& NumberOfTimeSteps,
                             1,
                             MPI_INT,
                             1,
                            1,
                             MPI_COMM_WORLD);
                  // send the k value
                  MPI_Send(& KValueWorker,
                             1,
                             MPI_INT,
                             1,
                            1,
                             MPI_COMM_WORLD);
                  // // sort the array
                sort(toSortArray.begin(), toSortArray.end(),compareInterval);
                  for (int k=0;k< KValueWorker;k++)
                     {
                          // outfile << i<< " " << toSortArray[k].start << " " <<  toSortArray[k].end <<" "<< toSortArray[k].distance<< endl;
                outfile<< iteration<< " " << toSortArray[k].start << " " <<  toSortArray[k].end <<" "<< toSortArray[k].distance<< endl;
                //  outfile << i<< " " << toSortArray[k].start << " " <<  toSortArray[k].end <<" "<< toSortArray[k].distance<< endl;

                        //  MPI_Send(&iteration,
                        //             1,
                        //             MPI_INT,
                        //             1,
                        //             iteration,
                        //             MPI_COMM_WORLD);
                        // MPI_Send(&toSortArray[k].start,
                        //                        1,
                        //                        MPI_INT,
                        //                        1,
                        //                        iteration,
                        //                        MPI_COMM_WORLD);
                        // MPI_Send(&toSortArray[k].end,
                        //                      1,
                        //                   MPI_INT,
                        //                 1,
                        //                   iteration,
                        //                   MPI_COMM_WORLD);
                        //   MPI_Send(&toSortArray[k].distance,
                        //                      1,
                        //                      MPI_FLOAT,
                        //                     1,
                        //                      iteration,
                        //                     MPI_COMM_WORLD);


                      }




      }


  }
  // else if (myid==1)
  // {   int  n;  // the number of NumberOfTimeSteps
  //     MPI_Recv(& n,
  //                 1,
  //              MPI_INT,
  //             MPI_ANY_SOURCE,
  //               1,
  //           MPI_COMM_WORLD,
  //          MPI_STATUS_IGNORE);
  //   // receiving the k value.
  //   int  KValue;
  //   MPI_Recv(& KValue,
  //                   1,
  //                MPI_INT,
  //               MPI_ANY_SOURCE,
  //                 1,
  //             MPI_COMM_WORLD,
  //            MPI_STATUS_IGNORE);
  //   // cout<< "the value of k value is"<< KValue<<endl;
  //   // cout<< "iterations  is"<< n<<endl;
  //
  //   for (int p=0;p<n;p++)
  //   {   int iteration;
  //       int start;
  //       int end;
  //       float distance;
  //     for  (int y=0;y<KValue;y++)
  //     {
  //       MPI_Recv(&iteration,
  //                       1,
  //                    MPI_INT,
  //                   MPI_ANY_SOURCE,
  //                     p,
  //                 MPI_COMM_WORLD,
  //                MPI_STATUS_IGNORE);
  //      MPI_Recv(&start,
  //                      1,
  //                     MPI_INT,
  //                    MPI_ANY_SOURCE,
  //                    p,
  //                    MPI_COMM_WORLD,
  //                 MPI_STATUS_IGNORE);
  //   MPI_Recv(&end,
  //                 1,
  //             MPI_INT,
  //               MPI_ANY_SOURCE,
  //                 p,
  //                   MPI_COMM_WORLD,
  //                 MPI_STATUS_IGNORE);
  //
  // MPI_Recv(& distance,
  //           1,
  //         MPI_INT,
  //         MPI_ANY_SOURCE,
  //           p,
  //           MPI_COMM_WORLD,
  //         MPI_STATUS_IGNORE);
  //
  //
  //   //  cout<< iteration<< " " <<start << " " <<  end <<" "<< distance<< endl;
  //     }
  //
  //   }
  //   //cout<<"still waiting"<< endl;
  //
  // }
MPI_Finalize();
outfile.close();
  //add timing
  finish = MPI_Wtime();
  if (myid == 0) {double elapsed = (finish - start);
      printf("timed as: %f s (#threads = %d )\n", elapsed, nodenum);}
  return 0;
}

void split(const std::string& s, char delimiter,vector<int> &vect)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {

	   string s2 = "-";

	   	// check if it contains - and add the range
     if (strstr(token.c_str(),s2.c_str()))
	 {
		 // adding the range
		vector<int> a=splitRange(token,'-' );
		int i=a[0];
		int j=a[1];
	//	cout<< i<<endl;
		//cout<<j<< endl;

		for ( ; i<=j; i++ ) {
  			 vect.push_back(i);
			//cout<< i<<endl;
		}
		// iterate
	// 	int end_=a.back();
	//    int begin_=a.front();
	//    cout<<end_<endl;
	//    cout<<begin_<endl;

	 }
	 else
	 {
		int x = 0;
   	    stringstream geek(token);
		geek >> x;
		vect.push_back(x);
	    //cout<< x<<endl;
	 }


   }

}
std::vector<int> splitRange(const std::string& s, char delimiter)
{
   std::vector<int> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {	// object from the class stringstream
	  	int x = 0;
   	    stringstream geek(token);
		  geek >> x;
	    tokens.push_back(x);
		//cout<<x;




   }
   return tokens;
}

float distance(float x1, float y1,float z1, float x2, float y2, float z2)
 {
     // printf("%.6f", x1);

   float square_difference_x = (x2 - x1) * (x2 - x1);
   float square_difference_y = (y2 - y1) * (y2 - y1);
  float square_difference_z = (z2 - z1) * (z2 - z1);
   float sum = square_difference_x + square_difference_y+square_difference_z;

	float value = sqrt(sum);
	return value;
}

// Compares two intervals according to staring times.
bool compareInterval(Interval i1, Interval i2)
{
    return (i1.distance< i2.distance);
}
